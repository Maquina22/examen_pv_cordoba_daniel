
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DANIEL
 */

@Entity
@Table(name = "Estudiantes")
public class Estudiantes {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Nro_ID;
    private String nombre;
    private String apellido;    
    private String direccion;

    public Estudiantes() {
    }

    public Estudiantes(int Nro_ID, String nombre, String apellido, String direccion) {
        this.Nro_ID = Nro_ID;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
    }

    public int getNro_ID() {
        return Nro_ID;
    }

    public void setNro_ID(int Nro_ID) {
        this.Nro_ID = Nro_ID;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
