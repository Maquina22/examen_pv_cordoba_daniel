
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author DANIEL
 */

@Entity
@Table(name = "Prestamos")
public class Prestamos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID_Prestamo;
    private String fecha;
    
    @ManyToOne
    @JoinColumn(name = "Nro_ID")
    private Estudiantes estudiantes;
   
    @ManyToOne
    @JoinColumn(name = "ISBN")
    private Libros libros;

    public Prestamos() {
    }

    public Prestamos(int ID_Prestamo, String fecha, Estudiantes estudiantes, Libros libros) {
        this.ID_Prestamo = ID_Prestamo;
        this.fecha = fecha;
        this.estudiantes = estudiantes;
        this.libros = libros;
    }

    public int getID_Prestamo() {
        return ID_Prestamo;
    }

    public void setID_Prestamo(int ID_Prestamo) {
        this.ID_Prestamo = ID_Prestamo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Estudiantes getEstudiantes() {
        return estudiantes;
    }

    public void setEstudiantes(Estudiantes estudiantes) {
        this.estudiantes = estudiantes;
    }

    public Libros getLibros() {
        return libros;
    }

    public void setLibros(Libros libros) {
        this.libros = libros;
    }    
}
