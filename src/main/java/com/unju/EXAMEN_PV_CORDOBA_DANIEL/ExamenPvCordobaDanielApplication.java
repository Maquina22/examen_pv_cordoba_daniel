package com.unju.EXAMEN_PV_CORDOBA_DANIEL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamenPvCordobaDanielApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenPvCordobaDanielApplication.class, args);
	}

}
