
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Estudiantes;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author DANIEL
 */
public interface Ibibliotecaestudiantes {
    public List<Estudiantes>listar();
    public Optional<Estudiantes> listarId(int dni);
    public int save(Estudiantes e);
    public void delete(int Nro_ID);    
}
