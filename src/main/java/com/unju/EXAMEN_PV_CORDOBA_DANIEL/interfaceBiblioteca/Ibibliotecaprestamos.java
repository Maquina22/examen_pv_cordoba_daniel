
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Prestamos;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author DANIEL
 */
public interface Ibibliotecaprestamos {
    public List<Prestamos>listar();
    public Optional<Prestamos> listarId(int dni);
    public int save(Prestamos p);
    public void delete(int ID_Prestamo);    
}
