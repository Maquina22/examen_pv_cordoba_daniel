
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Libros;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author DANIEL
 */
public interface Ibibliotecalibros {
    public List<Libros>listar();
    public Optional<Libros> listarId(int dni);
    public int save(Libros l);
    public void delete(int ISBN);    
}
