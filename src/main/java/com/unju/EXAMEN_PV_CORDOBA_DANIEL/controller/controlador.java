
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.controller;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecaestudiantes;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecalibros;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecaprestamos;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Estudiantes;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Libros;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Prestamos;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author DANIEL
 */

@Controller
@RequestMapping
public class controlador {
    
    @Autowired
    private Ibibliotecaestudiantes estu;    
    @Autowired
    private Ibibliotecalibros lib;    
    @Autowired
    private Ibibliotecaprestamos pres;
    
    @RequestMapping("/index")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "index";
    }
    
    //* * * Estudiante * * *    
    //Listar
    @GetMapping("/listarestudiantes")
    public String listarEstudiantes(Model model) {
        List<Estudiantes> estudiante = estu.listar();  
        model.addAttribute("Estudiantes", estudiante);
        return "estudiantes";
    }
    
    //Eliminar
    @GetMapping("/eliminarestudiante/{Nro_ID}")
    public String eliminarEstudiante(@PathVariable int Nro_ID,Model model) {
        estu.delete(Nro_ID);
        return "redirect:/listarestudiantes";
    }
    
    //Editar
    @GetMapping("/editarestudiante/{Nro_ID}")    
    public String editarEstudiante(@PathVariable int Nro_ID, Model model){
        Optional<Estudiantes> estudiante = estu.listarId(Nro_ID);
        model.addAttribute("Estudiantes", estudiante);
        return "frmEstudiante";
    }
        
    //Agregar 
    @GetMapping("/agregarestudiante")
    public String agregarEstudiante(Model model) {
        model.addAttribute("Estudiantes", new Estudiantes());
        return "frmEstudiante";
    }

    @PostMapping("/save")
    public String save(@Valid Estudiantes e,Model model) {
        estu.save(e);
        return "redirect:/listarestudiantes";
    }
           
    //* * * * * Prestamos * * * * *    
    //Listar
    @GetMapping("/listarprestamos")
    public String listarPrestamos(Model model) {
        List<Prestamos> prestamos = pres.listar();  
        model.addAttribute("Prestamos", prestamos);
        return "prestamos";
    }
    
    //Eliminar
    @GetMapping("/eliminarprestamo/{ID_Prestamo}")
    public String eliminarPrestamo(@PathVariable int ID_Prestamo,Model model) {
        pres.delete(ID_Prestamo);
        return "redirect:/listarprestamos";
    }   
    
    //Agregar 
    @GetMapping("/agregarprestamo")
    public String agregarPrestamo(Model model) {
        model.addAttribute("Prestamos", new Prestamos());
        return "frmPrestamo";
    }

    @PostMapping("/save1")
    public String save1(@Valid Prestamos p,Model model) {
        pres.save(p);
        return "redirect:/listarprestamos";
    }
    
    //Editar
    @GetMapping("/editarprestamo/{ID_Prestamo}")  
    public String editarPrestamo(@PathVariable int ID_Prestamo, Model model){
        Optional<Prestamos> prestamo = pres.listarId(ID_Prestamo);
        model.addAttribute("Estudiantes", prestamo);
        return "frmPrestamo";
    }
    
    
    //* * * * * Libros * * * * *    
    //Listar
    @GetMapping("/listarlibros")
    public String listarLibros(Model model) {
        List<Libros> libros = lib.listar();  
        model.addAttribute("Libros", libros);
        return "libros";
    }
    //Eliminar
    @GetMapping("/eliminarlibro/{ISBN}")
    public String eliminarLibro(@PathVariable int ISBN,Model model) {
        lib.delete(ISBN);
        return "redirect:/listarlibros";
    }
    
    //Agregar
    @GetMapping("/agregarlibro")
    public String agregarLibro(Model model) {
        model.addAttribute("Libros", new Libros());
        return "frmLibro";
    }

    @PostMapping("/save2")
    public String save2(@Valid Libros l,Model model) {
        lib.save(l);
        return "redirect:/listarlibros";
    }
    
    //Editar
    @GetMapping("/editarlibro/{ISBN}")    
    public String editarLibro(@PathVariable int ISBN, Model model){
        Optional<Libros> libro = lib.listarId(ISBN);
        model.addAttribute("Libros", libro);
        return "frmLibro";
    }

}
