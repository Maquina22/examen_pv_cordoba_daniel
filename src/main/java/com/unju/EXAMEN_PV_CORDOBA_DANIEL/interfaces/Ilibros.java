
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Libros;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DANIEL
 */
@Repository
public interface Ilibros extends CrudRepository <Libros, Integer>{
    
}
