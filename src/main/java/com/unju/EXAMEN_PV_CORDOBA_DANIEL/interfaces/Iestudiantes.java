
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Estudiantes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DANIEL
 */
@Repository
public interface Iestudiantes extends CrudRepository <Estudiantes, Integer>{
    
}
