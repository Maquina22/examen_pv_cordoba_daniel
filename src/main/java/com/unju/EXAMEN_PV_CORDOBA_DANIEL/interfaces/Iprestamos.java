
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Prestamos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DANIEL
 */
@Repository
public interface Iprestamos extends CrudRepository<Prestamos, Integer>{
    
}
