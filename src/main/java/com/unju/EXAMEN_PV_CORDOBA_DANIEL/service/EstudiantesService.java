
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.service;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecaestudiantes;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces.Iestudiantes;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Estudiantes;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DANIEL
 */

@Service
public class EstudiantesService implements Ibibliotecaestudiantes{
    @Autowired
    private Iestudiantes data;
    
    @Override
    public List<Estudiantes> listar() {
        return (List<Estudiantes>) data.findAll();
    }
    
    @Override
    public Optional<Estudiantes> listarId(int Nro_ID) {
       return data.findById(Nro_ID);
    }
    
    @Override
    public int save(Estudiantes e) {
        int res = 0;
        Estudiantes estudiantes = data.save(e);
        if (!estudiantes.equals(null)) {
            res = 1;
        }
        return res;
    }
    
    @Override
    public void delete(int Nro_ID) {
        data.deleteById(Nro_ID);
    }
}
