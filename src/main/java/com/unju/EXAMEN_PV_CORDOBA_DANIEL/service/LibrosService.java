
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.service;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecalibros;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces.Ilibros;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Libros;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DANIEL
 */

@Service
public class LibrosService implements Ibibliotecalibros{
    @Autowired
    private Ilibros data;
    
    @Override
    public List<Libros> listar() {
        return (List<Libros>) data.findAll();
    }
    
    @Override
    public Optional<Libros> listarId(int ISBN) {
       return data.findById(ISBN);
    }
    
    @Override
    public int save(Libros l) {
        int res = 0;
        Libros libros = data.save(l);
        if (!libros.equals(null)) {
            res = 1;
        }
        return res;
    }
    
    @Override
    public void delete(int ISBN) {
        data.deleteById(ISBN);
    }
}
