
package com.unju.EXAMEN_PV_CORDOBA_DANIEL.service;

import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaceBiblioteca.Ibibliotecaprestamos;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.interfaces.Iprestamos;
import com.unju.EXAMEN_PV_CORDOBA_DANIEL.modelo.Prestamos;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author DANIEL
 */

@Service
public class PrestamosService implements Ibibliotecaprestamos{
    @Autowired
    private Iprestamos data;
    
    @Override
    public List<Prestamos> listar() {
        return (List<Prestamos>) data.findAll();
    }
    
    @Override
    public Optional<Prestamos> listarId(int ID_Prestamo) {
       return data.findById(ID_Prestamo);
    }
    
    @Override
    public int save(Prestamos p) {
        int res = 0;
        Prestamos prestamos = data.save(p);
        if (!prestamos.equals(null)) {
            res = 1;
        }
        return res;
    }
    
    @Override
    public void delete(int ID_Prestamo) {
        data.deleteById(ID_Prestamo);
    }
}
